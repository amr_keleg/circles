﻿using UnityEngine;
using System.Collections;

public class RotateAround : MonoBehaviour {
	private float speed;
	private int addspeed;
	private int maxspeed=100;
	public Transform target; // the object to rotate around
		void Start() {
		if (target == null) 
		{
			target = this.gameObject.transform;
			Debug.Log ("RotateAround target not specified. Defaulting to parent GameObject");
		}
		Random.seed = Time.frameCount;
		addspeed += Random.Range (10, 20) * Random.Range (-1,1);
		if (addspeed > maxspeed)
			addspeed = maxspeed;
		else if (addspeed < -maxspeed)
			addspeed = -maxspeed;
		
	}

	// Update is called once per frame
	void Update () {
		// RotateAround takes three arguments, first is the Vector to rotate around
		// second is a vector that axis to rotate around
		// third is the degrees to rotate, in this case the speed per second
		addspeed += Random.Range (10, 20) * Random.Range (-1,1);
		if (addspeed > maxspeed)
			addspeed = maxspeed;
		else if (addspeed < -maxspeed)
			addspeed = -maxspeed;
		transform.RotateAround (target.transform.position, target.transform.forward, addspeed * Time.deltaTime);
	}
}
