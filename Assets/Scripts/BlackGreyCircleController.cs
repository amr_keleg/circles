﻿using UnityEngine;
using System.Collections;

public class BlackGreyCircleController : MonoBehaviour {

	private Rigidbody2D rg2d;
	public bool upplayer;
	public float speed;
	private float waitTime = 3f;
	private bool doneWaiting = false;
	void Start () {
		rg2d = GetComponent<Rigidbody2D> ();
	}

	void returnToOriginalPosition(bool nextstep=false)
	{
		if (nextstep) {
			rg2d.position += new Vector2 (0, 1.5f);
			rg2d.velocity = new Vector2 (0, 0);
		} else {
			if (upplayer) {
				rg2d.position = new Vector2 (0, 6.75f);
				rg2d.velocity = new Vector2 (0, 0);
			} else {
				rg2d.position = new Vector2 (0, -6.75f);
				rg2d.velocity = new Vector2 (0, 0);
			}

		}

	}
	// Update is called once per frame
	void Update () {
		// Wait until waitTime is below or equal to zero.
		if(waitTime > 0) {
			waitTime -= Time.deltaTime;
		} else {
			// Done.
			doneWaiting = true;
		}

	}
	//private bool moving_up = false;
	void FixedUpdate()
	{
		Vector2 movement=new Vector2 (0, 0);

		if (upplayer) {
			if (Input.GetKey (KeyCode.A)) {
				rg2d.velocity=new Vector2(0,-10);
				Invoke("stop", 0.2f);

				//moving_up = false;
				rg2d.AddForce (movement * speed);
			}
		} 
		else {
			if (Input.GetKeyDown (KeyCode.L) && doneWaiting) {
				//movement = new Vector2 (0, 10);
				RaycastHit2D hit = Physics2D.Raycast(transform.position+new Vector3(0,0.7f,0), new Vector2(0,1));
				if (hit.collider != null) {
					float distance = Mathf.Abs(hit.point.y - transform.position.y);
					Debug.Log (hit.collider.name);
					Debug.Log (distance);
					doneWaiting = false;
					waitTime = 3f;
					if (distance < 1.5f) {
						returnToOriginalPosition ();
					} else {
						returnToOriginalPosition (true);
					}

				}
			}
		}
		//Use the two store floats to create a new Vector2 variable movement.
	
		//Call the AddForce function of our Rigidbody2D rb2d supplying movement multiplied by speed to move our player.

	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (upplayer) {
			rg2d.position = new Vector2 (0, 7f);
			rg2d.velocity=new Vector2 (0,0);
		}
		else {
			}

	}
	void stop(){
		rg2d.velocity=new Vector2(0,0);
	}

}
