﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class upPlayerController : MonoBehaviour {
	private int score;
	public Text scoreRef;
	public float WAIT_TIME;
	private Rigidbody2D rg2d;
	public Rigidbody2D otherplayer;
	private float waitTime;
	private bool ignoreInput = false;
	public AudioSource winaudio,collisionaudio;
	private Vector2 originalPosition = new Vector2 (0,8.25f);
	void Start () {
		rg2d = GetComponent<Rigidbody2D> ();
		waitTime = WAIT_TIME;
		score = 0;
		scoreRef.text = score.ToString ();
	}


	void move()
	{
		rg2d.position += new Vector2 (0,-1.5f);
		insideCircle ();
	}
	void insideCircle()
	{
		if (Mathf.Abs(rg2d.position.y) < 3f) {
			score++;
			rg2d.position = originalPosition;
			otherplayer.position = -originalPosition;
			winaudio.Play();
			winaudio.Play(44100);
		}

	}
	void returnToOriginalPosition()
	{
		rg2d.position = originalPosition;
	}
	// Update is called once per frame
	void Update () {
		// Wait until waitTime is below or equal to zero.
		if(waitTime > 0) {
			waitTime -= Time.deltaTime;
		} else {
			// Done.
			ignoreInput = false;
		}
		scoreRef.text = score.ToString ();
	}
	//private bool moving_up = false;
	void FixedUpdate()
	{
		bool android = Input.touchCount> 0 && Input.GetTouch (0).phase == TouchPhase.Began;
		if ((Input.GetKey (KeyCode.A) ||android && (1.0*Input.GetTouch(0).position.y/Screen.height>0.5)) && !ignoreInput) {
			RaycastHit2D hit = Physics2D.Raycast(transform.position+new Vector3(0,-0.7f,0), new Vector2(0,-1));
			if (hit.collider != null) {
				float distance = Mathf.Abs(hit.point.y - transform.position.y);
				Debug.Log (hit.collider.name);
				Debug.Log (distance);
				ignoreInput = true;
				waitTime = WAIT_TIME;
				if (distance < 1.5f) {
					returnToOriginalPosition ();
					collisionaudio.Play();
					collisionaudio.Play(44100);
				} else {
					move ();
				}

			}		

		} 

	}


}
